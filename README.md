# study
学习路上的md文件

- [golang 语言](https://gitee.com/Skyd188/study-registry/tree/master/GO%E8%AF%AD%E8%A8%80)
- [linux常用命令](https://gitee.com/Skyd188/study-registry/tree/master/linux)
- [redis总结](https://gitee.com/Skyd188/study-registry/blob/master/nosql/redis%E5%91%BD%E4%BB%A4.md)
- [docker容器](https://gitee.com/Skyd188/study-registry/tree/master/docker%E4%BB%A5%E5%8F%8A%E6%9C%8D%E5%8A%A1%E7%BD%91%E6%A0%BC%E5%8C%96)
- 其他一些文件，稍后整理。可以看代码栏目

这是一份学习路上的成长文件。希望自己可以一直坚持写下去！

